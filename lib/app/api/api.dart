import 'package:dio/dio.dart';
import 'package:flutter_jarvi/app/api/models/course.dart';
import 'package:retrofit/retrofit.dart';

part 'api.g.dart';

@RestApi(baseUrl: '')
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  factory ApiClient.create({String? apiUrl}) {
    final dio = Dio();
    if (apiUrl != null) {
      return ApiClient(dio, baseUrl: apiUrl);
    }
    return ApiClient(dio);
  }

  @GET('/courses')
  Future<Courses> getCourses(@Query('search') String search);
}
