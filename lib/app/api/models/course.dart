import 'package:json_annotation/json_annotation.dart';

part 'course.g.dart';

@JsonSerializable()
class Course {
  final double? id;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? videoLink;
  final String? userAgreement;
  final double? formatId;
  final Product? product;

  Course({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.videoLink,
    this.userAgreement,
    this.formatId,
    this.product,
  });

  factory Course.fromJson(Map<String, dynamic> json) => _$CourseFromJson(json);
  Map<String, dynamic> toJson() => _$CourseToJson(this);
}

@JsonSerializable()
class Product {
  final double? id;
  final String? title;
  final String? description;
  final double? price;
  final double? discount;
  final DateTime? publishedAt;
  final double? discountPrice;
  final String? image;
  final double? superOrder;
  final double? order;
  final DateTime? deletedAt;
  final double? companyId;
  final double? publicationStatusId;
  final double? averageRate;

  Product({
    this.id,
    this.title,
    this.description,
    this.price,
    this.discount,
    this.publishedAt,
    this.discountPrice,
    this.image,
    this.superOrder,
    this.order,
    this.deletedAt,
    this.companyId,
    this.publicationStatusId,
    this.averageRate,
  });

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}

@JsonSerializable()
class Courses {
  final List<Course> courses;
  final double count;

  Courses({
    required this.courses,
    required this.count,
  });

  factory Courses.fromJson(Map<String, dynamic> json) => _$CoursesFromJson(json);
  Map<String, dynamic> toJson() => _$CoursesToJson(this);
}
