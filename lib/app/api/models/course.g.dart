// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'course.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Course _$CourseFromJson(Map<String, dynamic> json) => Course(
      id: (json['id'] as num?)?.toDouble(),
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      videoLink: json['videoLink'] as String?,
      userAgreement: json['userAgreement'] as String?,
      formatId: (json['formatId'] as num?)?.toDouble(),
      product: json['product'] == null
          ? null
          : Product.fromJson(json['product'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CourseToJson(Course instance) => <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'videoLink': instance.videoLink,
      'userAgreement': instance.userAgreement,
      'formatId': instance.formatId,
      'product': instance.product,
    };

Product _$ProductFromJson(Map<String, dynamic> json) => Product(
      id: (json['id'] as num?)?.toDouble(),
      title: json['title'] as String?,
      description: json['description'] as String?,
      price: (json['price'] as num?)?.toDouble(),
      discount: (json['discount'] as num?)?.toDouble(),
      publishedAt: json['publishedAt'] == null
          ? null
          : DateTime.parse(json['publishedAt'] as String),
      discountPrice: (json['discountPrice'] as num?)?.toDouble(),
      image: json['image'] as String?,
      superOrder: (json['superOrder'] as num?)?.toDouble(),
      order: (json['order'] as num?)?.toDouble(),
      deletedAt: json['deletedAt'] == null
          ? null
          : DateTime.parse(json['deletedAt'] as String),
      companyId: (json['companyId'] as num?)?.toDouble(),
      publicationStatusId: (json['publicationStatusId'] as num?)?.toDouble(),
      averageRate: (json['averageRate'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'price': instance.price,
      'discount': instance.discount,
      'publishedAt': instance.publishedAt?.toIso8601String(),
      'discountPrice': instance.discountPrice,
      'image': instance.image,
      'superOrder': instance.superOrder,
      'order': instance.order,
      'deletedAt': instance.deletedAt?.toIso8601String(),
      'companyId': instance.companyId,
      'publicationStatusId': instance.publicationStatusId,
      'averageRate': instance.averageRate,
    };

Courses _$CoursesFromJson(Map<String, dynamic> json) => Courses(
      courses: (json['courses'] as List<dynamic>)
          .map((e) => Course.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: (json['count'] as num).toDouble(),
    );

Map<String, dynamic> _$CoursesToJson(Courses instance) => <String, dynamic>{
      'courses': instance.courses,
      'count': instance.count,
    };
