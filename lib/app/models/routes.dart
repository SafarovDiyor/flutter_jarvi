class Routes {
  static const home = '/home';
  static const login = '/login';
  static const events = 'events';
  static const access = 'access';
  static const profile = 'profile';
  static const services = 'services';
  static const chat = 'chat';
  static const eventsPage = '/events';
  static const event = '/event/:id';
  static const rent = '/rent/:id';
}
