import 'package:flutter/material.dart';

class AppFonts {
  static const montserrat = 'Montserrat';
  static const dinpro = 'Dinpro';
}

class AppColors {
  static const primaryColor = Color(0xFF004734);
  static const fillColor = Color(0xFFF5F5F5);
  static const iconSelectedColor = Color(0xFF0D121C);
  static const iconUnselectedColor = Color(0xFFCFCFCF);
}

class AppThemes {
  static const inputDecorationTheme = InputDecorationTheme(
    filled: true,
    fillColor: AppColors.fillColor,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(12)),
      borderSide: BorderSide.none,
    ),
  );

  static const bottomNavigationBarTheme = BottomNavigationBarThemeData(
    backgroundColor: Colors.white,
    elevation: 0.0,
    selectedIconTheme: IconThemeData(
      color: AppColors.iconSelectedColor,
    ),
    selectedItemColor: AppColors.iconSelectedColor,
    type: BottomNavigationBarType.fixed,
    showUnselectedLabels: true,
    selectedLabelStyle: TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 13,
    ),
    unselectedLabelStyle: TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 13,
    ),
    enableFeedback: false,
    unselectedIconTheme: IconThemeData(color: AppColors.iconUnselectedColor),
    unselectedItemColor: AppColors.iconUnselectedColor,
  );

  static const bottomAppBarTheme = BottomAppBarTheme(
    color: Colors.white,
    elevation: 0.0,
    height: 72,
  );

  static const textTheme = TextTheme(
    headlineSmall: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      fontFamily: 'Montserrat',
      height: 2,
      fontSize: 11,
    ),
    labelSmall: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.w400,
      height: 0,
      fontFamily: 'Montserrat',
      fontSize: 12,
    ),
    headlineLarge: TextStyle(
      color: AppColors.primaryColor,
      fontWeight: FontWeight.w700,
      fontSize: 22,
      height: 0,
    ),
    headlineMedium: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontFamily: AppFonts.dinpro,
      height: 0,
      fontSize: 18,
    ),
  );
}
