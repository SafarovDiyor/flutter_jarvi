import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/routes.dart';
import 'package:flutter_jarvi/screens/access/access.dart';
import 'package:flutter_jarvi/screens/auth/auth.dart';
import 'package:flutter_jarvi/screens/chat/chat.dart';
import 'package:flutter_jarvi/screens/events/event/event.dart';
import 'package:flutter_jarvi/screens/events/events_screen.dart';
import 'package:flutter_jarvi/screens/events/rent/rent.dart';
import 'package:flutter_jarvi/screens/home/home.dart';
import 'package:flutter_jarvi/screens/profile/profile.dart';
import 'package:flutter_jarvi/screens/services/services.dart';
part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: AuthRoute.page, path: Routes.login, initial: true),
        AutoRoute(
            page: HomeRoute.page,
            path: Routes.home,
            initial: false,
            children: [
              AutoRoute(
                page: EventsRoute.page,
                path: Routes.events,
              ),
              AutoRoute(
                page: ChatRoute.page,
                path: Routes.chat,
              ),
              AutoRoute(
                page: AccessRoute.page,
                path: Routes.access,
              ),
              AutoRoute(
                page: ProfileRoute.page,
                path: Routes.profile,
              ),
              AutoRoute(
                page: ServicesRoute.page,
                path: Routes.services,
              ),
            ]),
        CustomRoute(
            path: Routes.event,
            page: SingleEventRoute.page,
            transitionsBuilder: TransitionsBuilders.slideLeft),
        CustomRoute(
            path: Routes.rent,
            page: SingleRentRoute.page,
            transitionsBuilder: TransitionsBuilders.slideLeft),
      ];
}
