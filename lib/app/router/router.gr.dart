// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    AccessRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const AccessPage(),
      );
    },
    AuthRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const AuthPage(),
      );
    },
    ChatRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ChatPage(),
      );
    },
    EventsRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const EventsPage(),
      );
    },
    HomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HomePage(),
      );
    },
    ProfileRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ProfilePage(),
      );
    },
    ServicesRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ServicesPage(),
      );
    },
    SingleEventRoute.name: (routeData) {
      final args = routeData.argsAs<SingleEventRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: SingleEventPage(
          key: args.key,
          id: args.id,
          img: args.img,
          info: args.info,
          time: args.time,
        ),
      );
    },
    SingleRentRoute.name: (routeData) {
      final args = routeData.argsAs<SingleRentRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: SingleRentPage(
          key: args.key,
          id: args.id,
          title: args.title,
        ),
      );
    },
  };
}

/// generated route for
/// [AccessPage]
class AccessRoute extends PageRouteInfo<void> {
  const AccessRoute({List<PageRouteInfo>? children})
      : super(
          AccessRoute.name,
          initialChildren: children,
        );

  static const String name = 'AccessRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [AuthPage]
class AuthRoute extends PageRouteInfo<void> {
  const AuthRoute({List<PageRouteInfo>? children})
      : super(
          AuthRoute.name,
          initialChildren: children,
        );

  static const String name = 'AuthRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ChatPage]
class ChatRoute extends PageRouteInfo<void> {
  const ChatRoute({List<PageRouteInfo>? children})
      : super(
          ChatRoute.name,
          initialChildren: children,
        );

  static const String name = 'ChatRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [EventsPage]
class EventsRoute extends PageRouteInfo<void> {
  const EventsRoute({List<PageRouteInfo>? children})
      : super(
          EventsRoute.name,
          initialChildren: children,
        );

  static const String name = 'EventsRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [HomePage]
class HomeRoute extends PageRouteInfo<void> {
  const HomeRoute({List<PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ProfilePage]
class ProfileRoute extends PageRouteInfo<void> {
  const ProfileRoute({List<PageRouteInfo>? children})
      : super(
          ProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'ProfileRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ServicesPage]
class ServicesRoute extends PageRouteInfo<void> {
  const ServicesRoute({List<PageRouteInfo>? children})
      : super(
          ServicesRoute.name,
          initialChildren: children,
        );

  static const String name = 'ServicesRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SingleEventPage]
class SingleEventRoute extends PageRouteInfo<SingleEventRouteArgs> {
  SingleEventRoute({
    Key? key,
    int? id,
    required String img,
    required String info,
    required String time,
    List<PageRouteInfo>? children,
  }) : super(
          SingleEventRoute.name,
          args: SingleEventRouteArgs(
            key: key,
            id: id,
            img: img,
            info: info,
            time: time,
          ),
          rawPathParams: {'id': id},
          initialChildren: children,
        );

  static const String name = 'SingleEventRoute';

  static const PageInfo<SingleEventRouteArgs> page =
      PageInfo<SingleEventRouteArgs>(name);
}

class SingleEventRouteArgs {
  const SingleEventRouteArgs({
    this.key,
    this.id,
    required this.img,
    required this.info,
    required this.time,
  });

  final Key? key;

  final int? id;

  final String img;

  final String info;

  final String time;

  @override
  String toString() {
    return 'SingleEventRouteArgs{key: $key, id: $id, img: $img, info: $info, time: $time}';
  }
}

/// generated route for
/// [SingleRentPage]
class SingleRentRoute extends PageRouteInfo<SingleRentRouteArgs> {
  SingleRentRoute({
    Key? key,
    double? id,
    required String title,
    List<PageRouteInfo>? children,
  }) : super(
          SingleRentRoute.name,
          args: SingleRentRouteArgs(
            key: key,
            id: id,
            title: title,
          ),
          rawPathParams: {'id': id},
          initialChildren: children,
        );

  static const String name = 'SingleRentRoute';

  static const PageInfo<SingleRentRouteArgs> page =
      PageInfo<SingleRentRouteArgs>(name);
}

class SingleRentRouteArgs {
  const SingleRentRouteArgs({
    this.key,
    this.id,
    required this.title,
  });

  final Key? key;

  final double? id;

  final String title;

  @override
  String toString() {
    return 'SingleRentRouteArgs{key: $key, id: $id, title: $title}';
  }
}
