import 'package:flutter/material.dart';

enum Place {
  bottomRight,
  bottomLeft,
  topLeft,
  topRight
}

class AbsoluteContainer extends StatelessWidget {
  final Place place;
  final Widget widget;
  const AbsoluteContainer({ super.key, required this.place, required this.widget });

  @override
  Widget build(BuildContext context){
    return Positioned(
      top: place == Place.topLeft || place == Place.topRight ? 0 : null,
      bottom: place == Place.bottomLeft || place == Place.bottomRight ? 0 : null,
      right: place == Place.bottomRight || place == Place.topRight ? 0 : null,
      left: place == Place.bottomLeft || place == Place.topLeft ? 0 : null,
      child: widget,
    );
  }
}