// ignore_for_file: use_full_hex_values_for_flutter_colors

import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';

class Button extends StatelessWidget {
  const Button(
      {super.key,
      required this.onPressed,
      required this.content,
      this.isDisable = false,
      this.textColor = Colors.white,
      this.backgroundColor = "0D121C",
      this.borderRadius = 12});

  final Widget content;
  final VoidCallback onPressed;
  final bool isDisable;
  final Color textColor;
  final double borderRadius;
  final String backgroundColor;

  Color getColor(String hexColor) {
    if (hexColor.isEmpty) {
      return AppColors.iconSelectedColor;
    } else {
      return Color(int.parse(hexColor.substring(1, 7), radix: 16) + 0xFF000000);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 48,
      constraints: const BoxConstraints(maxWidth: double.infinity),
      child: ElevatedButton(
        onPressed: isDisable ? null : onPressed,
        style: ElevatedButton.styleFrom(
          foregroundColor: textColor,
          backgroundColor:
              isDisable ? const Color(0xff21252952) : getColor(backgroundColor),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
        ),
        child: content,
      ),
    );
  }
}
