import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Close extends StatelessWidget {
  final double? top;
  final double? right;
  const Close({super.key, this.top, this.right});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top ?? 0,
      right: right ?? 0,
      child: IconButton(
        icon: SvgPicture.asset(
          Assets.close,
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }
}
