import 'package:flutter/material.dart';

class HomeDrag extends StatelessWidget {
  const HomeDrag({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Container(
        height: 4,
        width: 40,
        decoration: BoxDecoration(
            color: const Color(0xFF8C9295),
            borderRadius: BorderRadius.circular(6)),
      ),
    );
  }
}
