import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_jarvi/app/models/visual.dart';

class Input extends StatelessWidget {
  final Widget? icon;
  final Widget? bottomSheetContent;
  final double? iconWidth;
  final double? iconHeight;
  final String? hintText;
  final String? initialValue;
  final dynamic mask;
  final String? errorText;
  final bool? withArrowDown;
  final int? maxLength;
  final TextInputType inputType;
  final TextEditingController controller;

  double bottomPanelHeight(BuildContext context) {
    final double bottomPadding = MediaQuery.of(context).padding.bottom;
    return bottomPadding + 248;
  }

  const Input({
    super.key,
    this.icon,
    required this.inputType,
    this.iconWidth,
    this.mask,
    this.errorText,
    this.bottomSheetContent,
    this.initialValue,
    this.withArrowDown,
    this.iconHeight,
    this.hintText,
    this.maxLength,
    required this.controller,
  });

  List<TextInputFormatter> get inputFormatters {
    return mask != null ? [mask! as TextInputFormatter] : [];
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SizedBox(
      height: errorText!.isNotEmpty ? 90 : 66,
      child: TextFormField(
        textAlign: TextAlign.start,
        inputFormatters: inputFormatters,
        maxLength: maxLength ?? 150,
        keyboardType: inputType,
        maxLines: 1,
        style: const TextStyle(
            fontFamily: AppFonts.montserrat,
            fontSize: 17,
            fontWeight: FontWeight.w400),
        controller: controller,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(
              vertical: 20,
            ),
            prefixIconConstraints: BoxConstraints(
                maxHeight: iconHeight ?? 0.toDouble(),
                maxWidth: iconWidth ?? 24.toDouble()),
            prefixIcon: icon,
            hintText: hintText,
            hintStyle: theme.textTheme.labelSmall
                ?.copyWith(fontSize: 17, color: const Color(0xFFD0D0D0)),
            border: theme.inputDecorationTheme.border,
            fillColor: theme.inputDecorationTheme.fillColor,
            counterText: "",
            errorText: errorText,
            errorStyle: Theme.of(context).textTheme.labelSmall?.copyWith(
                fontSize: 13,
                color: const Color(0xFFFF3B30),
                fontFamily: AppFonts.montserrat)),
      ),
    );
  }
}
