// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/header_card/header_card.dart';

class HeaderWidget extends StatefulWidget {
  const HeaderWidget({super.key});

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  final List<String> items = ['1', '2', '3'];
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = MediaQuery.of(context).padding;
    double topToolbarHeight = padding.top;
    return Container(
        padding: EdgeInsets.only(top: topToolbarHeight + 16),
        color: Theme.of(context).primaryColor,
        child: Stack(children: [
          CarouselSlider(
            options: CarouselOptions(
              height: 196.0,
              viewportFraction: 1,
              autoPlay: false,
              enableInfiniteScroll: true,
              enlargeCenterPage: false,
              scrollDirection: Axis.horizontal,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              },
            ),
            items: items.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: const HeaderCard());
                },
              );
            }).toList(),
          ),
          Positioned(
            right: 0,
            left: 0,
            top: 210,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: items.map((item) {
                return Container(
                  width: 4.0,
                  height: 4.0,
                  margin: const EdgeInsets.symmetric(horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == items.indexOf(item)
                        ? Colors.white
                        : Colors.grey,
                  ),
                );
              }).toList(),
            ),
          ),
        ]));
  }
}
