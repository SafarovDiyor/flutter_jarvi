// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_jarvi/features/UI/home_drag/home.dart';

class DraggbleScrollableWidget extends StatefulWidget {
  const DraggbleScrollableWidget({
    super.key,
    required this.minChildSize,
    required this.maxChildSize,
    required this.titleWidget,
    required this.headerWidget,
    required this.listWidget,
    this.headerBottomBar,
  });
  final double maxChildSize;

  final double minChildSize;

  final Widget titleWidget;

  final Widget headerWidget;

  final Widget? headerBottomBar;

  final List<Widget> listWidget;
  @override
  _DraggbleScrollableWidgetState createState() =>
      _DraggbleScrollableWidgetState();
}

class _DraggbleScrollableWidgetState extends State<DraggbleScrollableWidget> {
  double _opacity = 0;
  // double _dragHeight = 0;
  @override
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _updateDragHeight();
  }

  void _updateDragHeight() {
    // setState(() {
    //   _dragHeight =
    //       (MediaQuery.of(context).size.height - 66) * widget.minChildSize;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Stack(
        children: [
          CustomScrollView(
            physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
            slivers: [
              MainSliver(opacity: _opacity, widget: widget),
            ],
          ),
          NotificationListener<DraggableScrollableNotification>(
            onNotification: (notification) {
              setState(() {
                _opacity = (notification.extent - widget.minChildSize) /
                    (widget.maxChildSize - widget.minChildSize);
                // _dragHeight = (MediaQuery.of(context).size.height - 66) *
                //     notification.extent;
              });
              return true;
            },
            child: MainDraggbleSheet(widget: widget),
          ),
        ],
      ),
    );
  }
}

class MainSliver extends StatelessWidget {
  const MainSliver({
    super.key,
    required double opacity,
    required this.widget,
  }) : _opacity = opacity;

  final double _opacity;
  final DraggbleScrollableWidget widget;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Theme.of(context).primaryColor,
      automaticallyImplyLeading: false,
      pinned: true,
      stretch: true,
      centerTitle: true,
      title: AnimatedOpacity(
        opacity: _opacity,
        duration: const Duration(milliseconds: 300),
        child: widget.titleWidget,
      ),
      expandedHeight: 550,
      flexibleSpace: Stack(
        children: [
          FlexibleSpaceBar(
            background: AnimatedOpacity(
              opacity: 1 - _opacity,
              duration: const Duration(milliseconds: 100),
              child: widget.headerWidget,
            ),
          ),
          if (widget.headerBottomBar != null) widget.headerBottomBar!,
        ],
      ),
    );
  }
}

class MainDraggbleSheet extends StatelessWidget {
  const MainDraggbleSheet({
    super.key,
    required this.widget,
  });

  final DraggbleScrollableWidget widget;

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      snap: true,
      snapAnimationDuration: const Duration(milliseconds: 250),
      initialChildSize: widget.minChildSize,
      minChildSize: widget.minChildSize,
      maxChildSize: widget.maxChildSize,
      builder: (context, scrollController) {
        return Column(
          children: [
            const HomeDrag(),
            const SizedBox(height: 8),
            Expanded(
              child: Container(
                padding: EdgeInsets.zero,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.0),
                    topRight: Radius.circular(12.0),
                  ),
                ),
                child: ListView(
                  padding: const EdgeInsets.only(top: 16.0),
                  physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
                  controller: scrollController,
                  children: widget.listWidget,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
