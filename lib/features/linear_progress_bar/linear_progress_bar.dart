import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter/material.dart';

class LinearProgressBarWidget extends StatelessWidget {
  final double lineHeight;
  final bool animation;
  final Radius barRadius;
  final EdgeInsets padding;
  final int animationDuration;

  const LinearProgressBarWidget({
    super.key,
    required this.animation,
    required this.lineHeight,
    required this.animationDuration,
    required this.padding,
    required this.barRadius, required int percent,
  });

  @override
  Widget build(BuildContext context) {
    return LinearPercentIndicator(
      animation: animation,
      lineHeight: lineHeight,
      backgroundColor: Colors.white,
      animationDuration: animationDuration,
      percent: 1,
      padding: padding,
      barRadius: barRadius,
      progressColor: const Color(0xFFF1BE53),
    );
  }
}
