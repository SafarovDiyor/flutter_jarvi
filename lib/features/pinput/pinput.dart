import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:pinput/pinput.dart';

class PinputWidget extends StatelessWidget {
  PinputWidget({super.key});

  final defaultPinTheme = PinTheme(
    width: 64,
    height: 80,
    textStyle: const TextStyle(
        fontSize: 28,
        fontFamily: AppFonts.dinpro,
        color: AppColors.iconSelectedColor,
        fontWeight: FontWeight.w700),
        decoration: BoxDecoration(
          border: Border.all(color: const Color.fromRGBO(234, 239, 243, 1)),
          borderRadius: BorderRadius.circular(12),
          color: AppColors.fillColor,
    ),
  );

  @override
  Widget build(BuildContext context) {
    final router = AutoRouter.of(context);
    return Pinput(
      onCompleted: (pin) {
        if (pin.length == 4) {
          router.navigateNamed('/home/events'); //заглушка
        }
      },
      defaultPinTheme: defaultPinTheme,
    );
  }
}
