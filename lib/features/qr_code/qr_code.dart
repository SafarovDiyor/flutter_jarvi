// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCodeWidget extends StatefulWidget {
  const QrCodeWidget({super.key, required this.qrData}); // исправлено
  final String qrData;

  @override
  _QrCodeState createState() => _QrCodeState();
}

class _QrCodeState extends State<QrCodeWidget> {
  late String qrData;

  @override
  void initState() {
    super.initState();
    qrData = widget.qrData;
  }
  @override
  Widget build(BuildContext context) {
    return Center(
      child: QrImageView(
        data: qrData,
        version: QrVersions.auto,
        size: 350,
        padding: const EdgeInsets.only(top: 40, left: 20),
        dataModuleStyle: const QrDataModuleStyle(
            color: AppColors.iconSelectedColor,
            dataModuleShape: QrDataModuleShape.circle),
        gapless: true,
        eyeStyle: const QrEyeStyle(
            eyeShape: QrEyeShape.radius, radius: 12, color: AppColors.iconSelectedColor),
      ),
    );
  }
}
