import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/app/view.dart';
import 'package:flutter_jarvi/screens/services/bloc/courses_list_bloc.dart';
import 'package:fvp/fvp.dart' as fvp;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: '.env');
  final client = ApiClient.create(apiUrl: dotenv.env['API_URL']);
  fvp.registerWith(); // для работы видео на Windows
  runApp(JarviApp(
    client: client,
  )); // Основной метод вызова всего приложения
}

class JarviApp extends StatefulWidget {
  final ApiClient client;
  const JarviApp({super.key, required this.client});

  @override
  State<JarviApp> createState() => _JarviAppState();
}

// используем цвета из AppColors
// используем темы из AppThemes
class _JarviAppState extends State<JarviApp> {
  final _router = AppRouter();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CoursesListBloc(apiClient: widget.client),
      child: MaterialAppWidget(router: _router),
    );
  }
}

class MaterialAppWidget extends StatelessWidget {
  const MaterialAppWidget({
    super.key,
    required AppRouter router,
  }) : _router = router;

  final AppRouter _router;

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        primaryColor: AppColors.primaryColor,
        colorScheme: ColorScheme.fromSeed(
          seedColor: AppColors.primaryColor,
        ),
        useMaterial3: true,
        inputDecorationTheme: AppThemes.inputDecorationTheme,
        bottomNavigationBarTheme: AppThemes.bottomNavigationBarTheme,
        bottomAppBarTheme: AppThemes.bottomAppBarTheme,
        textTheme: AppThemes.textTheme,
      ),
      routerConfig: _router.config(),
    );
  }
}
