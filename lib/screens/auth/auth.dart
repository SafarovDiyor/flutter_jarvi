import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/widgets/auth_screen/auth_fullscreen.dart';
import 'package:video_player/video_player.dart';

@RoutePage()
class AuthPage extends StatefulWidget {
  const AuthPage({super.key});

  @override
  State<AuthPage> createState() => AuthScreenState();
}

class AuthScreenState extends State<AuthPage> {

late VideoPlayerController controller;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: const AuthPageWidget(),
      )
    );
  }
}