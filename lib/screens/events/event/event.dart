import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/widgets/event_screen/event_screen.dart';

@RoutePage()
class SingleEventPage extends StatelessWidget {
  final int? id;
  final String img;
  final String info;
  final String time;
  const SingleEventPage({
    super.key,
    @PathParam('id') this.id,
    required this.img,
    required this.info,
    required this.time,
  });

  @override
  Widget build(BuildContext context) {
    return EventScreen(
      img: img,
      info: info,
      time: time,
    );
  }
}
