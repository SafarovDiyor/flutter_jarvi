import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/event_screen.dart';

@RoutePage()
class EventsPage extends StatefulWidget {
  const EventsPage({super.key});

  @override
  State<EventsPage> createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  @override
  Widget build(BuildContext context) {
    return EventScreenWidget(
      deviceHeight: MediaQuery.of(context).size.height +
          MediaQuery.of(context).padding.top,
    );
  }
}
