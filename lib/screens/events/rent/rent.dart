import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/widgets/rent_screen/rent_screen.dart';

@RoutePage()
class SingleRentPage extends StatelessWidget {
  final String title;
  final double? id;
  const SingleRentPage({
    super.key,
    @PathParam('id') this.id,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return RentScreen(
      title: title,
    );
  }
}
