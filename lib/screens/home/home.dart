import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/app/router/router.dart';
import 'package:flutter_svg/flutter_svg.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => HomeScreen();
}

class HomeScreen extends State<HomePage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter.tabBar(
      routes: routesArray,
      builder: (context, child, controller) {
        final tabsRouter = AutoTabsRouter.of(context);
        _currentIndex = tabsRouter.activeIndex;
        return PopScope(
            canPop: false,
            child: Scaffold(
              body: child,
              bottomNavigationBar: bottomAppBarWidget(tabsRouter),
            ));
      },
    );
  }

  List<PageRouteInfo<dynamic>> get routesArray {
    return const [
      EventsRoute(),
      ChatRoute(),
      AccessRoute(),
      ProfileRoute(),
      ServicesRoute()
    ];
  }

  BottomAppBar bottomAppBarWidget(TabsRouter tabsRouter) {
    return BottomAppBar(
                elevation: 0.0,
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _buildIconButton(
                      index: 0,
                      name: "Главная",
                      iconPath: _currentIndex == 0
                          ? Assets.homeActive
                          : Assets.homeInactive,
                      onPressed: () => _changeTab(0, tabsRouter),
                    ),
                    _buildIconButton(
                      index: 1,
                      name: "Чат",
                      iconPath: _currentIndex == 1
                          ? Assets.chatActive
                          : Assets.chatInactive,
                      onPressed: () => _changeTab(1, tabsRouter),
                    ),
                    _buildIconButton(
                      index: 2,
                      name: "Доступы",
                      iconPath: _currentIndex == 2
                          ? Assets.accessActive
                          : Assets.accessInactive,
                      onPressed: () => _changeTab(2, tabsRouter),
                    ),
                    _buildIconButton(
                      index: 3,
                      name: "Профиль",
                      iconPath: _currentIndex == 3
                          ? Assets.profileActive
                          : Assets.profileInactive,
                      onPressed: () => _changeTab(3, tabsRouter),
                    ),
                    _buildIconButton(
                      index: 4,
                      name: "Услуги",
                      iconPath: _currentIndex == 4
                          ? Assets.servicesActive
                          : Assets.servicesInactive,
                      onPressed: () => _changeTab(4, tabsRouter),
                    ),
                  ],
                ));
  }

  Widget _buildIconButton({
    required int index,
    required String name,
    required String iconPath,
    required VoidCallback onPressed,
  }) {
    return GestureDetector(
        onTap: () {
          onPressed();
        },
        child: SizedBox(
          width: 58.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                child: SvgPicture.asset(
                  iconPath,
                  width: 24,
                  height: 24,
                ),
              ),
              Text(
                name,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 13,
                  fontFamily: AppFonts.dinpro,
                  color: _currentIndex == index
                      ? AppColors.iconSelectedColor
                      : AppColors.iconUnselectedColor,
                ),
              ),
            ],
          ),
        ));
  }

  void _changeTab(int index, TabsRouter tabsRouter) {
    tabsRouter.setActiveIndex(index);
  }
}
