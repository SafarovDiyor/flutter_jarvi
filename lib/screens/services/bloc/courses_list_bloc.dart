// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_jarvi/app/api/models/course.dart';
import 'package:flutter_jarvi/app/view.dart';
import 'package:meta/meta.dart';

part 'courses_list_event.dart';
part 'courses_list_state.dart';

class CoursesListBloc extends Bloc<CoursesListEvent, CoursesListState> {
  CoursesListBloc({required this.apiClient}) : super(CoursesListInitial()) {
    on<SearchCourses>(_onSearch);
  }

  Future<void> _onSearch(
    SearchCourses event,
    Emitter<CoursesListState> emit,
  ) async {
    try {
      emit(CoursesListLoading());
      final courses = await apiClient.getCourses(event.query);
      emit(CoursesListLoaded(courses));
      if (courses.courses.isEmpty) {
        emit (CoursesListEmpty());
      }
    } catch (e) {
      emit(CoursesListFailure(e));
    }
  }

  final ApiClient apiClient;
}
