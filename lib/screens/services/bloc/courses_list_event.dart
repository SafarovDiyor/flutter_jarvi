part of 'courses_list_bloc.dart';

@immutable
sealed class CoursesListEvent extends Equatable {
  const CoursesListEvent();

  @override
  List<Object> get props => [];
}

class SearchCourses extends CoursesListEvent {
  const SearchCourses({required this.query});
  final String query;

  @override
  List<Object> get props => super.props..addAll([query]);
}
