part of 'courses_list_bloc.dart';

@immutable
sealed class CoursesListState extends Equatable {
  const CoursesListState();

  @override
  List<Object> get props => [];
}

final class CoursesListInitial extends CoursesListState {}

final class CoursesListLoading extends CoursesListState {}

final class CoursesListEmpty extends CoursesListState {}

final class CoursesListLoaded extends CoursesListState {
  final Courses courses;

  const CoursesListLoaded(this.courses);

  @override
  List<Object> get props => super.props..add(courses);
}

final class CoursesListFailure extends CoursesListState {
  final Object error;

  const CoursesListFailure(this.error);

  @override
  List<Object> get props => super.props..add(error);
}
