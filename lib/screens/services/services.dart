import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/home_drag/home.dart';
import 'package:flutter_jarvi/screens/services/bloc/courses_list_bloc.dart';
import 'package:flutter_jarvi/shared/get_file.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

@RoutePage()
class ServicesPage extends StatefulWidget {
  const ServicesPage({super.key});

  @override
  State<ServicesPage> createState() => ServicesScreen();
}

class ServicesScreen extends State<ServicesPage> {
  final _controller = TextEditingController();
  @override
  void initState() {
    BlocProvider.of<CoursesListBloc>(context)
        .add(const SearchCourses(query: ''));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: GestureDetector(
        onTap: () {
          BlocProvider.of<CoursesListBloc>(context)
              .add(SearchCourses(query: _controller.value.text));
        },
        child: Container(
          height: 48,
          width: 48,
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(16)),
              color: AppColors.iconUnselectedColor),
          child: const Icon(Icons.search),
        ),
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          '',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: TextSpaceBarField(controller: _controller),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: BlocBuilder<CoursesListBloc, CoursesListState>(
        bloc: BlocProvider.of<CoursesListBloc>(context),
        builder: (context, state) {
          if (state is CoursesListLoaded) {
            return ListView.builder(
              padding: const EdgeInsets.all(16.0),
              physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
              itemCount: state.courses.courses.length,
              itemBuilder: (context, index) {
                final courses = state.courses.courses;
                return Container(
                  margin: const EdgeInsets.only(bottom: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: AppColors.iconUnselectedColor),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      showBarModalBottomSheet(
                          context: context,
                          topControl: const HomeDrag(),
                          builder: (context) {
                            return const Column(
                              children: [Text('sdsdsd')],
                            );
                          });
                    },
                    child: Container(
                      height: 75,
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Row(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            margin: const EdgeInsets.only(right: 12),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              image: DecorationImage(
                                image: NetworkImage(
                                  getFileUrl(
                                      courses[index].product?.image ?? ""),
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                courses[index].product?.title ?? "",
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          if (state is CoursesListEmpty) {
            return Center(
              child: Text('Ничего не найдено', style: Theme.of(context).textTheme.headlineLarge,),
            );
          }

          if (state is CoursesListFailure) {
            return Center(
              child: Text('Ошибка', style: Theme.of(context).textTheme.headlineLarge,),
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class TextSpaceBarField extends StatelessWidget {
  const TextSpaceBarField({
    super.key,
    required TextEditingController controller,
  }) : _controller = controller;

  final TextEditingController _controller;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          controller: _controller,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 0, horizontal: 16.0),
              hintText: 'Введите название...',
              fillColor: Colors.white,
              counterText: "",
              errorStyle: Theme.of(context).textTheme.labelSmall?.copyWith(
                  fontSize: 13,
                  color: const Color(0xFFFF3B30),
                  fontFamily: AppFonts.montserrat)),
        ),
      ),
    );
  }
}
