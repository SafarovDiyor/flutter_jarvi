String getCurrentDateInRussian(DateTime currentTime) {
  List<String> monthsInRussian = [
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря"
  ];

  int day = currentTime.day;
  int monthIndex = currentTime.month - 1;
  String month = monthsInRussian[monthIndex];

  return "$day $month";
}