import 'package:flutter_dotenv/flutter_dotenv.dart';

String getFileUrl(String url) {
  if (!dotenv.env.containsKey('API_URL')) {
    throw Exception('API_URL не определен в файле .env');
  }

  return '${dotenv.env['API_URL']}storage/$url';
}
