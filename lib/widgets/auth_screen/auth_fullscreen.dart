// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/button/button.dart';
import 'package:flutter_jarvi/features/UI/home_drag/home.dart';
import 'package:flutter_jarvi/features/UI/input/input.dart';
import 'package:flutter_jarvi/shared/email.dart';
import 'package:flutter_jarvi/widgets/auth_screen/entities/choose_country/choose_country.dart';
import 'package:flutter_jarvi/widgets/auth_screen/entities/enter_code/enter_code.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:video_player/video_player.dart';

enum AuthType {
  phone,
  email,
}

enum RegionType {
  russia,
  belarus,
}

class AuthPageWidget extends StatefulWidget {
  const AuthPageWidget({super.key});

  @override
  _AuthWidgetState createState() => _AuthWidgetState();
}

class _AuthWidgetState extends State<AuthPageWidget> {
  AuthType _authType = AuthType.phone;
  RegionType _regionType = RegionType.russia;
  bool _showErrorText = false;
  bool _inEnterCode = false;
  late VideoPlayerController _controller;
  TextEditingController _textController = TextEditingController();
  AuthType get activeAuthType => _authType;
  RegionType get activeRegionType => _regionType;
  bool get isShowErrorText => _showErrorText;
  bool get inEnterCode => _inEnterCode;

  void setAuthType(AuthType type) {
    setState(() {
      _authType = type;
      _textController.value = TextEditingValue.empty;
    });
  }

  void setAuthRegion(RegionType type) {
    setState(() {
      _regionType = type;
      _textController.value = TextEditingValue.empty;
    });
  }

  void setInEnterCode(bool type) {
    setState(() {
      _inEnterCode = type;
    });
  }

  void setShowError(bool show) {
    setState(() {
      _showErrorText = show;
    });
  }

  final List<Map<AuthType, String>> authListType = [
    {
      AuthType.phone: 'Телефон',
    },
    {
      AuthType.email: 'Почта',
    },
  ];

  double bottomPanelHeight(BuildContext context) {
    final double bottomPadding = MediaQuery.of(context).padding.bottom;
    return bottomPadding + 244;
  }

// '+# ### ### ## ##' : '+### ## ### ####',
  var maskFormatter = MaskTextInputFormatter(
    mask: '+7 ### ### ## ##',
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy,
  );

  var maskFormatterBelarus = MaskTextInputFormatter(
    mask: '+375 ## ### ####',
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy,
  );

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController(
      text: _regionType == RegionType.russia ? '+7' : '+395',
    );
    _controller = VideoPlayerController.asset(Assets.video)
      ..initialize().then((_) {
        setState(() {});
      })
      ..setLooping(true)
      ..play();
  }

  @override
  Widget build(BuildContext context) {
    var errorText = isShowErrorText == true
        ? _textController.value.text.isEmpty
            ? 'Заполните поле'
            : 'Неверный формат'
        : '';

    _textController.addListener(() {
      setShowError(false);
    });

    bool validaiton(String value) {
      if (_textController.value.text.isEmpty) {
        return false;
      }

      if (activeAuthType == AuthType.phone &&
          (activeRegionType == RegionType.russia ||
              activeRegionType == RegionType.belarus)) {
        return value.length >= 16;
      }

      if (activeAuthType == AuthType.email) {
        return regexEmail(_textController.value.text);
      }

      return true;
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: barrierLogoScreen(),
      bottomSheet: Container(
        padding: const EdgeInsets.all(20.0),
        height: bottomPanelHeight(context) + (isShowErrorText ? 10.0 : 0),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            topRight: Radius.circular(12.0),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            authTypeSetter(context),
            const SizedBox(height: 24),
            activeAuthType == AuthType.phone
                ? Input(
                    mask: activeRegionType == RegionType.russia
                        ? maskFormatter
                        : maskFormatterBelarus,
                    errorText: errorText,
                    icon: chooseCountryIcon(context),
                    iconHeight: 24.0,
                    inputType: TextInputType.phone,
                    iconWidth: 89.0,
                    hintText:
                        activeRegionType == RegionType.russia ? '+7' : '+375',
                    controller: _textController,
                  )
                : Input(
                    icon: Container(),
                    errorText: errorText,
                    inputType: TextInputType.emailAddress,
                    controller: _textController,
                  ),
            const SizedBox(height: 24),
            authButton(validaiton, errorText, context),
          ],
        ),
      ),
    );
  }

  Stack barrierLogoScreen() {
    return Stack(
      children: [
        SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: _controller.value.isInitialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: VideoPlayer(_controller),
                )
              : const CircularProgressIndicator(),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 330,
          child: SizedBox(
            width: 392,
            height: 92,
            child: SvgPicture.asset(Assets.jarviResort),
          ),
        ),
      ],
    );
  }

  Button authButton(bool Function(String value) validaiton, String errorText,
      BuildContext context) {
    return Button(
      onPressed: () {
        if (!validaiton(_textController.text)) {
          setShowError(true);
          return;
        }
        if (errorText.isNotEmpty) {
          return;
        }
        setInEnterCode(true);
        showBarModalBottomSheet(
          context: context,
          topControl: const HomeDrag(),
          // isScrollControlled: true,
          barrierColor: AppColors.iconSelectedColor.withOpacity(0.8),
          builder: (BuildContext context) {
            return EnterCode(
                authType: activeAuthType,
                value: _textController.value.text,
                onClose: () => {Navigator.of(context).pop()});
          },
        );
      },
      isDisable: isShowErrorText,
      backgroundColor: '#0D121C',
      content: Text(
        'Войти',
        style: Theme.of(context)
            .textTheme
            .headlineSmall
            ?.copyWith(fontSize: 17, fontWeight: FontWeight.w400),
      ),
    );
  }

  Row authTypeSetter(BuildContext context) {
    return Row(
      children: authListType.map((action) {
        final authType = action.keys.first;
        final authDescription = action[authType];
        return GestureDetector(
            onTap: () {
              setAuthType(authType);
              FocusScope.of(context).unfocus();
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Text(
                authDescription!,
                style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      color: authType == activeAuthType
                          ? AppColors.iconSelectedColor
                          : AppColors.iconUnselectedColor,
                    ),
              ),
            ));
      }).toList(),
    );
  }

  GestureDetector chooseCountryIcon(BuildContext context) {
    return GestureDetector(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(width: 0),
            SizedBox(
              width: 35,
              height: 40,
              child: SvgPicture.asset(
                _regionType == RegionType.russia
                    ? Assets.russia
                    : Assets.belarus,
                width: 24,
                height: 24,
              ),
            ),
            SizedBox(
              width: 12,
              height: 12,
              child: SvgPicture.asset(
                Assets.arrow,
              ),
            ),
            const SizedBox(width: 16),
            SizedBox(
              width: 1,
              child: SvgPicture.asset(
                Assets.divider,
              ),
            ),
          ],
        ),
        onTap: () {
          showBarModalBottomSheet(
            context: context,
            topControl: const HomeDrag(),
            barrierColor: AppColors.iconSelectedColor.withOpacity(0.8),
            builder: (BuildContext context) {
              return Container(
                height: 200,
                padding: const EdgeInsets.all(24),
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.0),
                    topRight: Radius.circular(12.0),
                  ),
                ),
                child: ChooseCountry(
                  authRegion: _regionType,
                  onTap: (type) {
                    setAuthRegion(type);
                    Navigator.pop(context);
                  },
                ),
              );
            },
          );
        });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
