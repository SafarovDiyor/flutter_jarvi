import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/widgets/auth_screen/entities/choose_country/country_item/country_item.dart';
import 'package:flutter_jarvi/widgets/auth_screen/auth_fullscreen.dart';

class ChooseCountry extends StatelessWidget {
  ChooseCountry({
    super.key,
    required this.authRegion,
    required this.onTap,
  });

  final RegionType authRegion;
  final Function(RegionType) onTap;

  final List<Map<String, dynamic>> countries = [
    {
      'icon': Assets.russia,
      'name': 'Российская Федерация',
      'authRegion': RegionType.russia,
    },
    {
      'icon': Assets.belarus,
      'name': 'Республика Беларусь',
      'authRegion': RegionType.belarus,
    },
  ];

  static get russia => null;

  static get belarus => null;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Страна",
          style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                fontSize: 22,
                fontWeight: FontWeight.w700,
                color: AppColors.iconSelectedColor,
              ),
        ),
        const SizedBox(
          height: 8,
        ),
        SizedBox(
          height: 101, //refactor
          child: ListView.separated(
            itemCount: countries.length,
            separatorBuilder: (context, index) => const Divider(
              color: AppColors.fillColor,
              height: 1,
            ),
            itemBuilder: (context, index) {
              final action = countries[index];
              return Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      onTap(action['authRegion']);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: CountryItem(
                        icon: action['icon'] ?? '',
                        name: action['name'] ?? '',
                        // onTap: onTap,
                        active: action['authRegion'] == authRegion,
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ],
    );
  }
}
