import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CountryItem extends StatelessWidget {
  const CountryItem({
    super.key,
    required this.icon,
    required this.name,
    required this.active,
  });

  final bool active;
  final String name;
  final String icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          child: SvgPicture.asset(icon),
        ),
        const SizedBox(
          width: 16,
        ),
        Text(
          name,
          style: Theme.of(context).textTheme.labelSmall?.copyWith(
                fontSize: 17,
              ),
        ),
      ],
    );
  }
}
