// ignore_for_file: library_private_types_in_public_api

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/close/close.dart';
import 'package:flutter_jarvi/features/pinput/pinput.dart';
import 'package:flutter_jarvi/widgets/auth_screen/auth_fullscreen.dart';

class EnterCode extends StatefulWidget {
  const EnterCode({
    super.key,
    required this.authType,
    required this.value,
    required this.onClose,
  });

  final AuthType authType;
  final String value;
  final Function onClose;

  @override
  _EnterCodeState createState() => _EnterCodeState();
}

class _EnterCodeState extends State<EnterCode> {
  late Timer _timer;
  int _secondsRemaining = 10; // Время в секундах

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_secondsRemaining > 0) {
          _secondsRemaining--;
        } else {
          _timer.cancel();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    String finalText = widget.authType == AuthType.phone
        ? 'Мы отправили сообщение на номер \n${widget.value}'
        : 'Мы отправили письмо на почту \n${widget.value}';

    return Container(
      padding: MediaQuery.of(context).viewInsets,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: 310,
            padding: const EdgeInsets.all(24),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12.0),
                topRight: Radius.circular(12.0),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Введите код",
                      style:
                          Theme.of(context).textTheme.headlineMedium?.copyWith(
                                fontSize: 22,
                                fontWeight: FontWeight.w700,
                                color: AppColors.iconSelectedColor,
                              ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      finalText,
                      style: Theme.of(context).textTheme.labelSmall?.copyWith(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: const Color(0xFF7C7F84),
                          ),
                    ),
                  ],
                ),
                const SizedBox(height: 40),
                Column(
                  children: [
                    PinputWidget(),
                    const SizedBox(height: 24),
                    if (_secondsRemaining > 0)
                      RichText(
                        text: TextSpan(
                          style:
                              Theme.of(context).textTheme.labelSmall?.copyWith(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: const Color(0xFF7C7F84),
                                  ),
                          children: [
                            const TextSpan(
                              text: 'Отправить ещё раз ',
                            ),
                            TextSpan(
                              text:
                                  '0:${_secondsRemaining.toString().padLeft(2, '0')}',
                              style: const TextStyle(
                                color: AppColors.iconSelectedColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                    if (_secondsRemaining <= 0)
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _secondsRemaining = 10;
                            _startTimer();
                          });
                        },
                        child: Text(
                          'Отправить ещё раз',
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 13,
                              fontFamily: AppFonts.montserrat),
                        ),
                      )
                  ],
                ),
              ],
            ),
          ),
          const Close()
        ],
      ),
    );
  }
}
