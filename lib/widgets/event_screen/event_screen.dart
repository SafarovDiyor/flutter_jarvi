import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/draggble_home_widget/draggble_home_widget.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/event_title_bar/event_title_bar.dart';

class EventScreen extends StatelessWidget {
  final String info;
  final String img;
  final String time;

  const EventScreen(
      {super.key, required this.info, required this.img, required this.time});

  @override
  Widget build(BuildContext context) {
    return DraggableHome(
      title: SizedBox(
        width: double.infinity,
        child: EventTitleBar(
        name: info,
        time: time,
      ),), 
      alwaysShowLeadingAndAction: true,
      leading: IconButton(
        onPressed: Navigator.of(context).pop,
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
      ),
      headerWidget: Hero(
        tag: img,
        child: Image.asset(
          img,
          fit: BoxFit.cover,
        ),
      ),
      curvedBodyRadius: 12,
      headerExpandedHeight: 0.40,
      backgroundColor: Colors.white,
      physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
      appBarColor: AppColors.iconSelectedColor,
      body: [
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                info,
                style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                    color: const Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.w600,
                    fontSize: 22),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                'Не следует, однако, забывать о том, что курс на социально-ориентированный национальный проект в значительной степени обуславливает создание дальнейших направлений развития проекта! Повседневная практика показывает, что выбранный нами инновационный путь влечет за собой процесс внедрения и модернизации существующих финансовых и административных условий. Таким образом, дальнейшее развитие различных форм деятельности требует от нас системного анализа ключевых компонентов планируемого обновления.',
                style: Theme.of(context).textTheme.labelMedium?.copyWith(
                    color: const Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.w400,
                    fontSize: 17),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
