// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/arrow_left/arrow_left.dart';
import 'package:flutter_jarvi/shared/date.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/event_actions_widget/event_item/event_item.dart';

class EventActions extends StatefulWidget {
  final EdgeInsets? padding;

  const EventActions({super.key, this.padding});

  @override
  _EventActionsState createState() => _EventActionsState();
}

class _EventActionsState extends State<EventActions> {
  final List<Map<String, dynamic>> actionsData = [
    {
      'titleAction': 'Хатха-йога в малом зале',
      'time': '08:00-09:00',
      'id': 1,
      'img': Assets.yoga,
      'onTap': () {},
    },
    {
      'titleAction': 'Катание на лыжах',
      'time': '12:00-14:00',
      'id': 2,
      'img': Assets.ski,
      'onTap': () {},
    },
  ];

  bool startAnimation = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(const Duration(milliseconds: 400), () {
        setState(() {
          startAnimation = true;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: widget.padding ?? EdgeInsets.zero,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text.rich(
                TextSpan(
                  text: 'События',
                  style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 22,
                      ),
                  children: [
                    TextSpan(
                      text: ' / ${getCurrentDateInRussian(DateTime.now())}',
                      style:
                          Theme.of(context).textTheme.headlineMedium?.copyWith(
                                color: AppColors.iconUnselectedColor,
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                              ),
                    ),
                  ],
                ),
              ),
              const ArrowLeft()
            ],
          ),
        ),
        const SizedBox(height: 24),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          child: ListView.separated(
              shrinkWrap: true,
              padding: widget.padding ?? EdgeInsets.zero,
              physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
              itemCount: actionsData.length,
              separatorBuilder: (context, index) => const SizedBox(height: 24),
              itemBuilder: (context, index) {
                return animatedItem(index);
              }),
        ),
      ],
    );
  }

  Widget animatedItem(int index) {
    return AnimatedContainer(
      curve: Curves.easeInOut,
      transform: Matrix4.translationValues(
          startAnimation ? 0 : MediaQuery.of(context).size.width, 0, 0),
      duration: Duration(milliseconds: 500 + (index * 100)),
      child: EventItem(
        titleAction: actionsData[index]['titleAction'] ?? '',
        time: actionsData[index]['time'] ?? '',
        img: actionsData[index]['img'] ?? '',
        id: actionsData[index]['id'] ?? '',
      ),
    );
  }
}
