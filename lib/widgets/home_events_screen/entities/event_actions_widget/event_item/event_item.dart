import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/view.dart';

class EventItem extends StatelessWidget {
  const EventItem({
    super.key,
    required this.titleAction,
    required this.time,
    required this.id,
    required this.img,
  });

  final String titleAction;
  final String time;
  final String img;
  final int id;

  @override
  Widget build(BuildContext context) {
    final router = AutoRouter.of(context);
    return GestureDetector(
      onTap: () {
        router.push(SingleEventRoute(id: id, info: titleAction, img: img, time: time));
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Hero(
            tag: img,
            child: Image.asset(
              img,
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          Text(
            titleAction,
            style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                color: Colors.black, fontWeight: FontWeight.w400, fontSize: 16),
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            time,
            style: Theme.of(context).textTheme.labelMedium?.copyWith(
                color: Colors.black, fontWeight: FontWeight.w400, fontSize: 13),
          )
        ],
      ),
    );
  }
}
