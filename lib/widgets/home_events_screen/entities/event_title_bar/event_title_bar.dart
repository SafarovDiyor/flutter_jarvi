import 'package:flutter/material.dart';

class EventTitleBar extends StatelessWidget {
  final String name;
  final String time;
  const EventTitleBar({super.key, required this.name, required this.time});

  @override
  Widget build(BuildContext context) {
    return Title(name: name, time: time);
  }
}

class Title extends StatelessWidget {
  const Title({
    super.key,
    required this.name,
    required this.time,
  });

  final String name;
  final String time;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name,
          style: Theme.of(context).textTheme.headlineMedium?.copyWith(
              color: const Color.fromARGB(255, 255, 255, 255),
              fontWeight: FontWeight.w600,
              fontSize: 22),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          time,
          style: Theme.of(context).textTheme.labelSmall?.copyWith(
              color: const Color.fromARGB(255, 255, 255, 255),
              fontWeight: FontWeight.w400,
              fontSize: 13),
        ),
      ],
    );
  }
}
