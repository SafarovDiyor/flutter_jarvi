import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/button/button.dart';
import 'package:flutter_jarvi/features/UI/home_drag/home.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:percent_indicator/percent_indicator.dart';

class HeaderCard extends StatelessWidget {
  const HeaderCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0), color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '№ 302',
                style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                      color: Colors.black,
                      fontSize: 22,
                    ),
              ),
              const SizedBox(height: 4),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '19 янв — 2 фев',
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: Colors.black,
                          fontSize: 13,
                        ),
                  ),
                  Text(
                    'Код от двери 450 258',
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: Colors.black,
                          fontSize: 13,
                        ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(height: 12),
          Button(
            onPressed: () {
              showDialog(
                context: context,
                barrierColor: AppColors.iconSelectedColor.withOpacity(0.8),
                builder: (BuildContext context) {
                  return Dialog(
                    backgroundColor: Colors.transparent,
                    child: Container(
                      color:
                          Colors.transparent, // Устанавливаем прозрачный цвет
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: CircularPercentIndicator(
                              animation: true,
                              radius: 50,
                              lineWidth: 10,
                              animationDuration: 2500,
                              onAnimationEnd: () {
                                Navigator.of(context).pop();
                              },
                              percent: 1,
                              progressColor: const Color(0xFFF1BE53),
                            ),
                          ),
                          const SizedBox(height: 8),
                          Text(
                            'Открыть номер',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            },
            backgroundColor: '#0D121C',
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 16,
                  height: 16,
                  child: SvgPicture.asset(Assets.wifi),
                ),
                const SizedBox(width: 8),
                const Text("Открыть номер")
              ],
            ),
          ),
          const SizedBox(height: 8),
          Button(
            onPressed: () {
              showBarModalBottomSheet(
                context: context,
                topControl: const HomeDrag(),
                bounce: true,
                builder: (context) {
                  return Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 2,
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Список услуг',
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge
                                ?.copyWith(color: Colors.black, fontSize: 32),
                          ),
                         const SizedBox(height: 8,),
                          SizedBox(
                            height: 420,
                            child: ListView.builder(
                              itemCount: 20,
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: 8),
                                  child: Row(mainAxisAlignment:  MainAxisAlignment.spaceBetween,children: [Text('Услуга $index', style: Theme.of(context).textTheme.bodyLarge,), Icon(Icons.arrow_circle_right)],),
                                ); 
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            },
            backgroundColor: '#FAEDDA',
            content: const Text("Заказать услуги"),
            textColor: Colors.black,
          ),
        ],
      ),
    );
  }
}
