import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NameWidget extends StatelessWidget {
  const NameWidget(
      {super.key, required this.name, required this.amount, this.padding});
  final String name;
  final int amount;
  final EdgeInsets? padding;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? EdgeInsets.zero,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                name,
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Colors.black, fontSize: 22),
              ),
              SizedBox(
                width: 17,
                height: 17,
                child: SvgPicture.asset(Assets.col),
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 16,
                height: 16,
                child: SvgPicture.asset(Assets.star),
              ),
              const SizedBox(
                width: 8,
              ),
              Text(
                '$amount бонусов',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall
                    ?.copyWith(color: Colors.black, fontSize: 13),
              ),
            ],
          )
        ],
      ),
    );
  }
}
