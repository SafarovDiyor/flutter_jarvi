import 'package:flutter/material.dart';

class RentItem extends StatelessWidget {
  const RentItem({super.key, required this.rentTitle});

  final String rentTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
     decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0), color: const Color(0xFFFAEEDA)),
      width: 114,
      height: 80,
      padding: const EdgeInsets.all(16.0),
      child:  Text(
        rentTitle,
        style: Theme.of(context).textTheme.headlineMedium?.copyWith(
            color: Colors.black, fontWeight: FontWeight.w600, fontSize: 14),
      ),
    );
  }
}
