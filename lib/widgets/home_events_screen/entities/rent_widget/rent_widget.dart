// ignore_for_file: use_key_in_widget_constructors

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/router/router.dart';
import 'package:flutter_jarvi/features/UI/arrow_left/arrow_left.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/rent_widget/rent_item/rent_item.dart';
class RentWidget extends StatelessWidget {
  final EdgeInsets? padding;
  const RentWidget({Key? key, this.padding});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
          padding: padding ?? EdgeInsets.zero,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text.rich(
                TextSpan(
                  text: 'Аренда',
                  style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 22),
                ),
              ),
              const ArrowLeft()
            ],
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        RentList(
          padding: padding ?? EdgeInsets.zero,
        ),
      ],
    );
  }
}

class RentList extends StatelessWidget {
  final List<Map<String, String>> actionsData = [
    {
      'rentTitle': 'Спортивный инвентарь',
    },
    {
      'rentTitle': 'Готовим на природе',
    },
    {
      'rentTitle': 'Отдых на озере',
    },
    {
      'rentTitle': 'Что-то ещё',
    },
  ];
  final EdgeInsets? padding;

  RentList({Key? key, this.padding});

  @override
  Widget build(BuildContext context) {
    final router = AutoRouter.of(context);
    return SizedBox(
      height: 80,
      width: MediaQuery.of(context).size.width,
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          padding: padding ?? EdgeInsets.zero,
          physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
          primary: true,
          itemCount: actionsData.length,
          separatorBuilder: (context, index) => const SizedBox(width: 12),
          itemBuilder: (context, index) => AnimatedOpacity(
              opacity: 1.0,
              duration: Duration(seconds: index * 2),
              child: GestureDetector(
                onTap: () {
                  router.push(SingleRentRoute(
                      id: index.toDouble(),
                      title: actionsData[index]['rentTitle'] ?? ""));
                },
                child: RentItem(
                  rentTitle: actionsData[index]['rentTitle'] ?? '',
                ),
              ))),
    );
  }
}
