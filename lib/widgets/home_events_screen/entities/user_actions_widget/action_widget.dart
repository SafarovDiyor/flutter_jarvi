import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/absolute_container/absolute_container.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ActionWidget extends StatelessWidget {
  const ActionWidget({
    super.key,
    required this.titleWidget,
    required this.annotationWidget,
    required this.iconUrl,
    required this.onTap,
    this.long,
  });

  final String titleWidget;
  final String annotationWidget;
  final String iconUrl;
  final bool? long;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
          if (long == true) {
         onTap();
        }
 
      },
      onTap: () {
        if (long == false) {
         onTap();
        }
      },
      child: Container(
        margin: EdgeInsets.zero,
        padding: const EdgeInsets.all(16.0),
        height: 120,
        width: (MediaQuery.of(context).size.width - 48)/2,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: AppColors.fillColor,
        ),
        child: Stack(
          fit: StackFit.loose,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  titleWidget,
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Colors.black, fontSize: 16),
                ),
                const SizedBox(height: 4.0),
                LimitedBox(
                  maxWidth: 143,
                  maxHeight: 36,
                  child: Text(
                    annotationWidget,
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontFamily: AppFonts.montserrat,
                      color: Colors.black,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            AbsoluteContainer(
              place: Place.bottomRight,
              widget: SizedBox(
                width: 36,
                height: 36,
                child: SvgPicture.asset(iconUrl),
              ),
            )
          ],
        ),
      ),
    );
  }
}
