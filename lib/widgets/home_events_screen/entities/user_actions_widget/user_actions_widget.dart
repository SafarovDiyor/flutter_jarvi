import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/assets.dart';
import 'package:flutter_jarvi/app/models/visual.dart';
import 'package:flutter_jarvi/features/UI/close/close.dart';
import 'package:flutter_jarvi/features/UI/home_drag/home.dart';
import 'package:flutter_jarvi/features/qr_code/qr_code.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'action_widget.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'dart:math';

class UserActions extends StatelessWidget {
  final EdgeInsets? padding;
  const UserActions({super.key, this.padding});

  String getRandomString(int length) {
    const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    final random = Random();
    return String.fromCharCodes(Iterable.generate(length,
        (_) => characters.codeUnitAt(random.nextInt(characters.length))));
  }

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> actionsData = [
      {
        'title_widget': 'Открыть шлагбаум',
        'long': true,
        'annotation_widget': 'Нажмите и держите',
        'icon_url': Assets.finger,
        'onTap': () {
          showDialog(
            context: context,
            barrierColor: AppColors.iconSelectedColor.withOpacity(0.8),
            builder: (BuildContext context) {
              return Dialog(
                backgroundColor: Colors.transparent,
                child: Container(
                  color: Colors.transparent, // Устанавливаем прозрачный цвет
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: LinearPercentIndicator(
                          animation: true,
                          lineHeight: 30.0,
                          backgroundColor: Colors.white,
                          animationDuration: 2500,
                          onAnimationEnd: () {
                            Navigator.of(context).pop();
                          },
                          percent: 1,
                          padding: const EdgeInsets.all(4.0),
                          barRadius: const Radius.circular(100.0),
                          progressColor: const Color(0xFFF1BE53),
                        ),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        'Открыть шлагбаум',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
      },
      {
        'title_widget': 'Завтрак включен',
        'long': false,
        'annotation_widget': '7:00-11:00',
        'icon_url': Assets.qr,
        'onTap': () {
          showBarModalBottomSheet(
            context: context,
            topControl: const HomeDrag(),
            barrierColor: AppColors.iconSelectedColor.withOpacity(0.8),
            builder: (BuildContext context) {
              return Container(
                width: double.infinity,
                height: 525,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.0),
                    topRight: Radius.circular(12.0),
                  ),
                ),
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    const Close(),
                    Column(
                      children: [
                        QrCodeWidget(qrData: getRandomString(30)),
                        const SizedBox(
                          height: 24,
                        ),
                        Text(
                          textAlign: TextAlign.center,
                          'Ваш QR-код\nдля входа в ресторан',
                          style: Theme.of(context).textTheme.headlineLarge,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Text(
                          textAlign: TextAlign.center,
                          'Поднесите к сканеру и дождитесь\nподтверждения',
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall
                              ?.copyWith(
                                  fontFamily: AppFonts.montserrat,
                                  fontSize: 15,
                                  color: const Color(0xFF7C7F84)),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                      ],
                    )
                  ],
                ),
              );
            },
          );
        },
      },
    ];
    return Padding(
      padding: padding ?? EdgeInsets.zero,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: actionsData.map((action) {
          return ActionWidget(
              titleWidget: action['title_widget'] ?? '',
              long: action['long'],
              annotationWidget: action['annotation_widget'] ?? '',
              iconUrl: action['icon_url'] ?? '',
              onTap: action['onTap'] ?? () {},
            );
        }).toList(),
      ),
    );
  }
}
