// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_jarvi/features/draggble_scrollable_widget/draggble_scrollable_widget.dart';
import 'package:flutter_jarvi/features/carousel_widget/carousel_widget.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/event_actions_widget/event_actions_widget.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/name_widget/name_widget.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/rent_widget/rent_widget.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/title_top/title_top.dart';
import 'package:flutter_jarvi/widgets/home_events_screen/entities/user_actions_widget/user_actions_widget.dart';

class EventScreenWidget extends StatefulWidget {
  final double deviceHeight;
  const EventScreenWidget({super.key, required this.deviceHeight});

  @override
  _EventScreenWidgetState createState() => _EventScreenWidgetState();
}

class _EventScreenWidgetState extends State<EventScreenWidget>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;
  final padding = const EdgeInsets.symmetric(horizontal: 16.0);

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 650),
    );

    _animation = Tween<double>(
      begin: 0,
      end: (widget.deviceHeight - 300) / widget.deviceHeight,
    ).animate(
        CurvedAnimation(parent: _animationController, curve: Curves.easeIn));
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _animation.addListener(() {
      debugPrint('Animation value: ${_animation.value}');
    });
    return AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          return AnimatedBuilder(
            animation: _animationController,
            builder: (context, child) {
              final double minChildSize = _animation.value;
              final double maxChildSize =
                  (widget.deviceHeight - 90) / widget.deviceHeight;
              return MainSheet(
                padding: padding,
                minChildSize: minChildSize,
                maxChildSize: maxChildSize,
                context: context,
              );
            },
          );
        });
  }
}

class MainSheet extends StatelessWidget {
  const MainSheet({
    super.key,
    required this.padding,
    required this.minChildSize,
    required this.maxChildSize,
    required this.context,
  });

  final EdgeInsets padding;
  final double minChildSize;
  final double maxChildSize;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return DraggbleScrollableWidget(
        minChildSize: minChildSize,
        maxChildSize: maxChildSize,
        titleWidget: const SafeArea(
          child: TitleTop(
            title: '№ 305',
            number: "Код от двери 405 465",
          ),
        ),
        headerWidget: const SafeArea(child: HeaderWidget(),),
        listWidget: [
          NameWidget(name: 'Name name', amount: 20, padding: padding),
          const SizedBox(
            height: 20,
          ),
          UserActions(padding: padding),
          const SizedBox(
            height: 32,
          ),
          EventActions(padding: padding),
          const SizedBox(
            height: 16,
          ),
          RentWidget(padding: padding),
          const SizedBox(
            height: 16,
          )
        ]);
  }
}
