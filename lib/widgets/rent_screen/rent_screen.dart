// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_jarvi/app/models/visual.dart';

class RentScreen extends StatefulWidget {
  final String title;
  const RentScreen({super.key, required this.title});

  @override
  _RentScreenState createState() => _RentScreenState();
}

class _RentScreenState extends State<RentScreen> {
  final String text =
      'Соображения высшего порядка, а также начало повседневной работы по формированию позиции способствует подготовке и реализации существующих финансовых и административных условий. Разнообразный и богатый опыт выбранный нами инновационный путь способствует повышению актуальности новых предложений. Значимость этих проблем настолько очевидна, что реализация намеченного плана развития играет важную роль в формировании ключевых компонентов планируемого обновления!';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            onPressed: Navigator.of(context).pop,
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          title: Hero(
            tag: widget.title,
            child: Text(
              widget.title,
              style: Theme.of(context).textTheme.labelMedium?.copyWith(
                    fontSize: 22,
                    color: Colors.white,
                  ),
            ),
          )),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(color: AppColors.iconSelectedColor),
        child: Text(
          text,
          style: Theme.of(context).textTheme.labelMedium?.copyWith(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.w300
              ),
        ),
      ),
    );
  }
}
