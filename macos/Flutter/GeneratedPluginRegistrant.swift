//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import fvp
import smart_auth
import video_player_avfoundation

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  FvpPlugin.register(with: registry.registrar(forPlugin: "FvpPlugin"))
  SmartAuthPlugin.register(with: registry.registrar(forPlugin: "SmartAuthPlugin"))
  FVPVideoPlayerPlugin.register(with: registry.registrar(forPlugin: "FVPVideoPlayerPlugin"))
}
